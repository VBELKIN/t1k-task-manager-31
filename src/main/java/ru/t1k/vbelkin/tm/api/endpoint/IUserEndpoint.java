package ru.t1k.vbelkin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.dto.request.*;
import ru.t1k.vbelkin.tm.dto.response.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request);

    @NotNull
    UserLockResponse lockUser(@NotNull final UserLockRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request);

    @NotNull
    UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request);

}
