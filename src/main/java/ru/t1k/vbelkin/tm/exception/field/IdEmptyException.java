package ru.t1k.vbelkin.tm.exception.field;

public class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
