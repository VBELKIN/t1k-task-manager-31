package ru.t1k.vbelkin.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.Task;

public class TaskRemoveByIndexResponse extends AbstractTaskResponse {

    public TaskRemoveByIndexResponse(@Nullable final Task task) {
        super(task);
    }

}
